export function workDuration () {
  const workDuration = {
    seconds: 0,
    minutes: 0,
    hours: 0,
    workdays: 0
  }

  workDuration.seconds = this.seconds()
  workDuration.minutes = this.minutes()

  const hours = Math.floor(this.asHours())
  workDuration.workdays = Math.floor(hours / 8)
  workDuration.hours = hours % 8

  return workDuration
}
