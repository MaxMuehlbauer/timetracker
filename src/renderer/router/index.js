import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'time-tracking',
      component: require('@/components/TimeTrackerView.vue').default
    },
    {
      path: '/stats/',
      name: 'stats',
      component: require('@/components/StatsView.vue').default
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
