import {Project} from '../interfaces/project'

const state = {
  projects: []
}

const getters = {
  activeProjects (state) {
    const arr = []

    for (const project of state.projects) {
      if (project.active) {
        arr.push(project)
      }
    }

    return arr
  },

  projectById (state) {
    return (id) => {
      return state.projects.find(project => {
        return project.id === id
      })
    }
  }
}

const mutations = {
  ADD_PROJECT (state, {name, opts}) {
    if (state.projects.findIndex(project => {
      return project.name === name
    }) >= 0) {
      console.warn(`Project ${name} already exists`)
      return
    }

    state.projects.push(new Project(name, opts))
  },

  REMOVE_PROJECT (state, {name}) {
    const foundProject = state.projects.find(project => {
      return project.name === name
    })

    if (foundProject === undefined) {
      console.warn(`Project ${name} does not exist`)
      return
    }

    foundProject.active = false
  }
}

const actions = {}

export default {
  state,
  mutations,
  actions,
  getters
}
