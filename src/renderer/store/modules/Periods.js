import {Period} from '../interfaces/period'

const state = {
  periods: []
}

const mutations = {
  'NEW_PERIOD' (state) {
    const index = state.periods.length
    const currentPeriod = getRunningPeriod(state)

    if (currentPeriod) {
      Period.stop(currentPeriod)
    }

    const newPeriod = new Period(index)

    state.periods.push(newPeriod)
  }
}

const getters = {
  runningPeriod (state) {
    return getRunningPeriod(state)
  }
}

const actions = {}

export default {
  state,
  mutations,
  getters,
  actions
}

function getRunningPeriod (state) {
  return state.periods.find(period => period.time.running)
}
