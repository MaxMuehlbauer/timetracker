import {Session} from '../interfaces/session'
import * as moment from 'moment'

const state = {
  sessions: []
}

const mutations = {
  NEW_SESSION (state, {project, opts, period}) {
    const session = new Session(project, opts, period)
    state.sessions.push(session)
    Session.start(session)
  },

  STOP_SESSION (state, session) {
    Session.stop(session)
  },

  SET_SESSION (state, {oldSession, newSession}) {
    const id = oldSession.id
    const session = state.sessions.find(session => {
      return session.id === id
    })

    session.opts.note = newSession.opts.note
    session.time.start = moment(newSession.time.start).toISOString()

    if (newSession.time.end && !newSession.time.running) {
      Session.stop(session, moment(newSession.time.end))
    }
  },

  DELETE_SESSION (state, deletedSession) {
    const index = state.sessions.findIndex((session) => session.id === deletedSession.id)

    state.sessions.splice(index, 1)
  }
}

const getters = {
  runningSessions (state) {
    return state.sessions.filter(session => {
      return session.time.running
    })
  },

  runningSessionsByProject (state, getters) {
    return (projectId) => {
      return getters.runningSessions.filter(session => {
        return session.project === projectId
      })
    }
  },

  sessionsByProjectAndPeriod (state) {
    return (projectId, periodId) => {
      return state.sessions.filter(session => session.project === projectId && session.period === periodId)
    }
  },

  runningSessionsByPeriod (state, getters) {
    return (periodId) => {
      return getters.runningSessions.filter(session => {
        return session.period === periodId
      })
    }
  },

  sessionsByPeriod (state) {
    return (periodId) => {
      return state.sessions.filter(session => {
        return session.period === periodId
      })
    }
  },

  sessionById (state) {
    return (id) => {
      return state.sessions.find(session => {
        return session.id === id
      })
    }
  }
}

const actions = {}

export default {
  state,
  mutations,
  actions,
  getters
}
