import * as uuid from 'uuid/v1'
import {Session} from './session'
import * as moment from 'moment'

export class Period {
  constructor (index) {
    this.id = uuid()
    this.index = index
    this.time = {
      running: false
    }

    Period.start(this)
  }

  static start (period, startTime = moment()) {
    period.time.start = Session.roundTime(startTime)
    period.time.running = true
  }

  static stop (period, endTime = moment()) {
    period.time.end = Session.roundTime(endTime)
    period.time.running = false
  }
}
