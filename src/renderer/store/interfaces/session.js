import * as uuid from 'uuid/v1'
import * as moment from 'moment'

export class Session {
  constructor (project, opts, period) {
    this.id = uuid()
    this.project = project
    this.period = period
    this.opts = {...Session.defaultOpts(this), ...opts}
    this.time = {
      running: false
    }

    Session.start(this)
  }

  static start (session, startTime = moment()) {
    session.time.start = Session.roundTime(startTime).toISOString()
    session.time.running = true
  }

  static stop (session, endTime = moment()) {
    session.time.end = Session.roundTime(endTime).toISOString()
    session.time.running = false
    Session.updateDuration(session, session.time.end)
  }

  static updateDuration (session, targetTime = moment()) {
    session.time.duration = moment.duration(moment(session.time.end).diff(moment(session.time.start))).toISOString()
  }

  static defaultOpts (session) {
    return {
      note: `Session #${session.id}`
    }
  }

  static roundTime (time) {
    let newTime = time

    const minuteOffset = time.minutes() % 5

    if (minuteOffset <= 2) {
      newTime.startOf('minute').subtract(minuteOffset, 'minutes')
    } else {
      newTime.startOf('minute').add(5 - minuteOffset, 'minutes')
    }

    return newTime
  }
}
