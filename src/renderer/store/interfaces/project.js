import * as uuid from 'uuid/v1'
import * as colorConvert from 'color-convert'

export class Project {
  constructor (name, opts) {
    this.name = name
    this.id = uuid()
    this.opts = {...Project.defaultOpts, ...opts}
    this.active = true
  }

  static getDarkerColor (color) {
    const hsl = colorConvert.hex.hsl(color)
    hsl[2] = hsl[2] - 25
    return `#${colorConvert.hsl.hex(...hsl)}`
  }
}

Project.defaultOpts = {
  color: '#6fb8b6'
}
